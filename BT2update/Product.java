package sample;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


public class Product {
    private SimpleStringProperty tenSP;
    private SimpleIntegerProperty maSP;

    private SimpleDoubleProperty giaBan;

    private SimpleIntegerProperty soLuongBan;
    private SimpleStringProperty Hang;


    public Product() {
    }

    //Constructor
    public Product(String tenSP, Integer maSP, Double giaNhap, Double giaBan, Integer soLuongBan, Integer soLuongNhap,
                   String Hang, String danhMuc) {
        this.tenSP = new SimpleStringProperty(tenSP);
        this.maSP = new SimpleIntegerProperty(maSP);
        this.giaBan = new SimpleDoubleProperty(giaBan);

        this.soLuongBan = new SimpleIntegerProperty(soLuongBan);

        this.Hang = new SimpleStringProperty(Hang);


    }

    //getter
    public String getTenSP() {
        return this.tenSP.get();
    }

    public void setTenSP(String tenSP) {
        this.tenSP = new SimpleStringProperty(tenSP);
    }

    public Integer getMaSP() {
        return this.maSP.get();
    }

    //Setter
    public void setMaSP(Integer maSP) {
        this.maSP = new SimpleIntegerProperty(maSP);
    }

    public Double getGiaBan() {
        return this.giaBan.get();
    }

    public void setGiaBan(Double giaBan) {
        this.giaBan.set(giaBan);
    }

    public Integer getSoLuongBan() {
        return this.soLuongBan.get();
    }

    public void setSoLuongBan(Integer soLuongBan) {
        this.soLuongBan.set(soLuongBan);
    }

    public String getHang() {
        return this.Hang.get();
    }

    public void setHang(String Hang) {
        this.Hang = new SimpleStringProperty(Hang);
    }


}



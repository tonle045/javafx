package sample;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class TableViewDemo extends Application {
    @Override
    public void start(Stage primaryStage1) throws Exception {
        TableView<Sanpham> table = new TableView<Sanpham>();

        // Tạo cột ma (Kiểu dữ liệu int)
        TableColumn<Sanpham, Integer> useMa = new TableColumn<Sanpham,Integer>("ma");
        TableColumn<Sanpham,String> useTen = new TableColumn<Sanpham,String>("Ten");
        TableColumn<Sanpham,Integer> useGiaBan = new TableColumn<Sanpham,Integer>("Gia ban");
        TableColumn<Sanpham,Integer> useSoLuong = new TableColumn<Sanpham,Integer>("so luong");
        TableColumn<Sanpham,String> useHang = new TableColumn<Sanpham,String>("hang");


        // Định nghĩa cách để lấy dữ liệu cho mỗi ô.
        useMa.setCellValueFactory(new PropertyValueFactory<>("ma"));
        useTen.setCellValueFactory(new PropertyValueFactory<>("ten"));
        useGiaBan.setCellValueFactory(new PropertyValueFactory<>("Gia_ban"));
        useSoLuong.setCellValueFactory(new PropertyValueFactory<>("so_luong"));
        useHang.setCellValueFactory(new PropertyValueFactory<>("hang"));

        // Hiển thị các dòng dữ liệu
        ObservableList<Sanpham> listSP = (ObservableList<Sanpham>) Db.getConnection();
        table.setItems(listSP);
        table.getColumns().addAll(useMa,useTen,useGiaBan,useSoLuong,useHang);

        StackPane root = new StackPane();
        root.setPadding(new Insets(5));
        root.getChildren().add(table);

        Scene scene = new Scene(root, 450, 300);
        primaryStage1.setScene(scene);
        primaryStage1.show();
    }

    public static void main(String[] args) {
        launch(args);}
}

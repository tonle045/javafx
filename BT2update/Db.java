package sample;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Db {

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        String connectionURL = "jdbc:mysql://localhost:3306/demo";
        return DriverManager.getConnection(connectionURL, "root", "root");
    }

    public static void insertSanpham(String ten, int ma, int soLuong, int giaBan, String hang) {
        String sql = "INSERT INTO san_pham(ten, ma, so_luong,Gia_ban, hang) " +
                "VALUES (?, ?, ?, ?, ?)";
        // Mo connection xuong db
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, ten);
            statement.setInt(2, ma);
            statement.setInt(3, soLuong);
            statement.setInt(4, giaBan);
            statement.setString(5, hang);
            // execute
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Sanpham> timSanphamTheoTen(String ten) {
        String sql = "select * from san_pham sp where ten like ?";
        List<Sanpham> ls = new ArrayList<>();
        // Mo connection xuong db
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "%" + ten + "%");

            // execute
            ResultSet rs = statement.executeQuery();

            while (rs.next()){
                Sanpham sanpham = new Sanpham();
                sanpham.setTen(rs.getString("ten"));
                sanpham.setGiaBan(rs.getInt("gia_ban"));
                sanpham.setSoLuong(rs.getInt("so_luong"));
                sanpham.setMa(rs.getInt("ma"));
                sanpham.setHang(rs.getString("hang"));
                System.out.println(rs.getString("hang"));
                ls.add(sanpham);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ls;
    }

    public static void main(String[] args) {
        timSanphamTheoTen("");
    }
}

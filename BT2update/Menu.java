package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.List;

public class Menu extends Application {

//    Auto resize


    private final GridPane gridPane = new GridPane();
    private final Label lbTitle = new Label("Quan ly sp ");
    private final TextField txtContent = new TextField();
    private final Button search = new Button("Tim kiem");
    private final Button add = new Button("Them SP");

//    ColumnConstraints column1 = new ColumnConstraints();
//        column1.setPercentWidth(40);
//    ColumnConstraints column2 = new ColumnConstraints();
//        column2.setPercentWidth(40);
//        .getColumnConstraints().addAll(column1, column2);

    public static void main(String[] args) {
        launch(args);
    }

    private TableView createProductTable() {
        TableView<Sanpham> tableView = new TableView<>();
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        TableColumn tbID = new TableColumn("Mã SP");
        TableColumn tbName = new TableColumn("Tên SP");
        TableColumn tbSellValue = new TableColumn("Giá Bán");
        TableColumn tbSellNumber = new TableColumn("Số lương Bán ");
        TableColumn tbBrand = new TableColumn("Hãng");

        tbName.setMinWidth(130);
        tbName.setCellValueFactory(new PropertyValueFactory<>("ten"));
        tbID.setMinWidth(70);
        tbID.setCellValueFactory(new PropertyValueFactory<>("ma"));
        tbSellValue.setMinWidth(150);
        tbSellValue.setCellValueFactory(new PropertyValueFactory<>("giaBan"));
        tbSellNumber.setMinWidth(150);
        tbSellNumber.setCellValueFactory(new PropertyValueFactory<>("soLuong"));
        tbBrand.setMinWidth(120);
        tbBrand.setCellValueFactory(new PropertyValueFactory<String, String>("hang"));
        tableView.getColumns().addAll(tbID, tbName, tbSellNumber, tbSellValue, tbBrand);
        tableView.setItems(getListSanpham());
        return tableView;
    }

    private ObservableList<Sanpham> getListSanpham(){
        List<Sanpham> sanphamList = Db.timSanphamTheoTen("");
        return FXCollections.observableArrayList(sanphamList);
    }

    @Override
    public void start(Stage primaryStage) {
        ScrollPane scrollPane = new ScrollPane();
        TableView tableView = createProductTable();
        scrollPane.setContent(tableView);
        scrollPane.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);

        primaryStage.setTitle("Tonle");
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        txtContent.setPromptText("Nhap noi dung tim kiem");
        createProductTable().setLayoutX(10);

        gridPane.add(lbTitle, 0, 0, 2, 1);
        gridPane.add(txtContent, 5, 3, 2, 2);
        gridPane.add(search, 8, 3, 2, 2);
        gridPane.add(add, 3, 0, 1, 1);

        VBox vBox = new VBox();
        vBox.getChildren().add(gridPane);
        vBox.getChildren().add(scrollPane);

        Scene scene = new Scene(vBox, 600, 300);
        primaryStage.setScene(scene);
        primaryStage.show();

        add.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {


                // Một cửa sổ mới (Stage)
                Stage newWindow = new Stage();
                newWindow.initOwner(primaryStage);
                newWindow.initModality(Modality.WINDOW_MODAL);
                newWindow.setTitle("Second Stage");
                newWindow.setX(primaryStage.getX() + 200);
                newWindow.setY(primaryStage.getY() + 100);

                // initial grid
                GridPane pane = new GridPane();
                pane.setAlignment(Pos.CENTER);
                pane.setHgap(10);
                pane.setVgap(10);
                pane.setPadding(new Insets(25, 25, 25, 25));

                Text sceneTitle = new Text("Them san pham");
                sceneTitle.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
                pane.add(sceneTitle, 0, 0, 2, 1);

                Label lbMaSP = new Label("MaSP ");
                pane.add(lbMaSP, 0, 1);
                TextField txtMaSP = new TextField();
                pane.add(txtMaSP, 1, 1);

                Label lbTenSP = new Label("TenSP ");
                pane.add(lbTenSP, 0, 2);
                TextField txtTenSP = new TextField();
                pane.add(txtTenSP, 1, 2);

                Label lbSLban = new Label("So luong ban ");
                pane.add(lbSLban, 0, 3);
                TextField txtSlban = new TextField();
                pane.add(txtSlban, 1, 3);

                Label lbGiaSP = new Label("Gia San pham ");
                pane.add(lbGiaSP, 0, 4);
                TextField txtGiaSP = new TextField();
                pane.add(txtGiaSP, 1, 4);

                Label lbHang = new Label("Hang ");
                pane.add(lbHang, 0, 5);
                TextField txtHang = new TextField();
                pane.add(txtHang, 1, 5);

                // Button them moi san pham

                Button btnThemMoi = new Button("Them moi");
                pane.add(btnThemMoi, 1, 6);
                btnThemMoi.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        // Update du lieu
                        Db.insertSanpham(txtTenSP.getText(),
                                Integer.parseInt(txtMaSP.getText()),
                                Integer.parseInt(txtSlban.getText()), Integer.parseInt(txtGiaSP.getText()),
                                txtHang.getText());
                        // Close new window
                        newWindow.close();

                    }
                });

                Scene secondScene = new Scene(pane, 300, 350);

                // gan content vao window
                newWindow.setScene(secondScene);
                newWindow.show();

//
            }
        });




    }
}
